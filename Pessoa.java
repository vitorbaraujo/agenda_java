import java.io.IOException;

public class Pessoa{

  private String nome;
  private String telefone;
  private String email;

  //caso nao se declare nenhum construtor, o construtor public Pessoa(){} e definido por default
  //caso contrario, o construtor default e sobrescrito

  public Pessoa(String nome){
    this.nome = nome;
  }

  public void setNome(String nome) throws IOException{
    this.nome = nome;
  }

  public String getNome(){
    return nome;
  }

  public void setTelefone(String telefone) throws IOException{
    this.telefone = telefone;
  }

  public String getTelefone(){
    return telefone;
  }

  public void setEmail(String email) throws IOException{
    this.email = email;
  }

  public String getEmail(){
    return email;
  }

/* pode-se tambem declarar metodos privados  
  private boolean validaTelefone(String telefone){

  }
*/

}
