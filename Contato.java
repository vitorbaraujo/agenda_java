public class Contato extends Pessoa{
  
  private String localDeTrabalho
  private String telefoneComercial;

  public Contato(String nome){
    super(nome);
  }

  public Contato(String nome,String localDeTrabalho){
    super(nome);
    this.localDeTrabalho = localDeTrabalho;
  }

  public void setLocalDeTrabalho(String localDeTrabalho){
    this.localDeTrabalho = localDeTrabalho;
  }

  public String getLocalDeTrabalho(){
    return localDeTrabalho;
  }

 // public void ...


}
