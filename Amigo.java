public class Amigo extends Pessoa{
   
  private String aniversario;
  private String facebook;

  public Amigo(String nome){
    super(nome); //chamada para o construtor da superclasse Pessoa
  }

  public Amigo(String nome, String aniversario){
    super(nome);
    this.aniversario = aniversario;
  }

  public void setAniversario(String aniversario){
    this.aniversario = aniversario;
  }

  public String getAniversario(){
    return aniversario;
  }

  public void setFacebook(String facebook){
    this.facebook = facebook;
  }

  public String getFacebook(){
    return facebook;
  }

}
