//importando classe Scanner, da API do Java
import java.util.Scanner;
import java.io.IOException;

public class Agenda{

 public static void main(String[] args){
  
   Scanner leitor = new Scanner(System.in); //instanciando um objeto (leitor) do tipo Scanner
   Pessoa umaPessoa; //declarou umaPessoa
   // Pode ser declarado e instanciado ao mesmo tempo:
   // Pessoa umaPessoa = new Pessoa(umNome);

   //try{

   System.out.println("Digite o nome: ");
   String umNome = leitor.nextLine();

   umaPessoa = new Pessoa(umNome); //instanciando umaPessoa apos entrar com o nome, pois o construtor precisa do nome

   System.out.println("Digite o telefone: ");
   String umTelefone = leitor.nextLine();
   //umaPessoa.setTelefone(leitor.nextLine());
   umaPessoa.setTelefone(umTelefone);

   System.out.println("Digite o email: ");
   String umEmail = leitor.nextLine();
   umaPessoa.setEmail(umEmail);

   //}catch(IOException erro){
    //System-out-println("Erro de entrada no sistema. Verifique seu teclado");
   //}

   System.out.println(umaPessoa.getNome()+"\n"+umaPessoa.getTelefone()+"\n"+umaPessoa.getEmail());

//   System.out.println("Nome: " + umaPessoa.getNome());
//   System.out.println("Telefone: " + umaPessoa.getTelefone());
//   System.out.println("Email: " + umaPessoa.getEmail());
 }

}
